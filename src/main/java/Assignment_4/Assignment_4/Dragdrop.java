package Assignment_4.Assignment_4;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.NoAlertPresentException;	
import org.openqa.selenium.Alert;
import java.util.concurrent.TimeUnit;

import java.util.List;
import java.util.concurrent.TimeUnit;
public class Dragdrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\dparuchuri002\\eclipse-workspace\\Assignment-4\\drivers\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://the-internet.herokuapp.com/drag_and_drop");
		
		driver.manage().window().maximize();
		
		WebElement drag1=driver.findElement(By.xpath("//div[@id='column-a']"));
		WebElement drag2=driver.findElement(By.xpath("//div[@id='column-b']"));
		Actions act=new Actions(driver);
		act.clickAndHold(drag1).moveToElement(drag2).release(drag1).perform();
		act.clickAndHold(drag2).moveToElement(drag1).release(drag2).perform();
		//act.release(drag1);
		//act.release(drag2);
		//act.dragAndDrop(driver.findElement(By.xpath("//div[@id='column-b']")), driver.findElement(By.xpath("//div[@id='column-a']"))).perform();
		
				//driver.close();
	}
}


