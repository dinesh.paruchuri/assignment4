package Assignment_4.Assignment_4;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.NoAlertPresentException;	
import org.openqa.selenium.Alert;

import java.util.List;
public class JS_Exctrs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\dparuchuri002\\eclipse-workspace\\Assignment-4\\drivers\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		
		driver.manage().window().maximize();
		
		WebElement alertButton=driver.findElement(By.xpath("//button[@onclick='jsAlert()']"));
		alertButton.click();
		Alert alert = driver.switchTo().alert();
		
		alert.accept();
		
				//driver.close();
	}
}


